<?php 
/*-------------------------------------------------------------------
    Template Name: Revision Search
-------------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/pages/header-page'); ?>

<?php get_template_part('template-parts/navigation/anchor'); ?>

<main>
	<article>
		<div id="default" class="anchor"></div>

		<?php if( !empty(get_the_content()) ) { ?>
			<section class="default-contents">
				<div class="block">
					<?php get_template_part('template-parts/elements/kits-search'); ?>

					<?php get_template_part('template-parts/pages/content', 'default'); ?>
				</div>
			</section>
		<?php } ?>

		<?php get_template_part('template-parts/elements/annies-kits'); ?>
		
	</article>
</main>

<?php get_footer(); ?>