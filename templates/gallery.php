<?php 
/*-------------------------------------------------------------------
    Template Name: Gallery
-------------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/pages/header-page'); ?>

<?php get_template_part('template-parts/navigation/anchor'); ?>

<main>
	<article>
		<div  id="default" class="anchor"></div>

		<?php //QUERY KITS
			$args = array( 
				'posts_per_page'  => -1, 
				'post_type' => 'kit',
				'meta_key'		=> 'gallery',
			);
			$kit_query = new WP_Query( $args );
		?>

		<section class="galleries">
			<h2>Sort by project</h2>
			<?php if ( $kit_query->have_posts() ) : ?>
				<select>
					<option value="*">All</option>
					<?php while ( $kit_query->have_posts() ) : $kit_query->the_post(); ?>
						<?php if( have_rows('gallery') ): ?>
								<option value=".<?php echo $post->post_name; ?>"><?php the_title(); ?></option>
						<?php endif; ?>
					<?php endwhile; ?>
				</select>
			<?php endif; ?>
			<div class="block">
				<?php if ( $kit_query->have_posts() ) : ?>
					<?php while ( $kit_query->have_posts() ) : $kit_query->the_post(); ?>
						<?php if( have_rows('gallery') ): ?>
								<div class="gallery-sizer"></div>
								<?php while ( have_rows('gallery') ) : the_row(); ?>
									<?php 
										$image = get_sub_field('image');
										$alt = $image['alt'];
										$preview = $image['sizes']['medium'];
									?>
									<a href="<?php echo $image['url']; ?>" class="gallery-image <?php the_sub_field('size'); ?> <?php echo $post->post_name; ?>">
										<img src="<?php echo $preview; ?>" alt="<?php echo $image['alt']; ?>" />
										<div class="overlay"></div>
										<p><?php the_title(); ?><br>
											<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="43" height="44">
												<defs>
													<path id="w" d="M933.18 1447.37a16.18 16.18 0 1 1 0-32.37 16.18 16.18 0 0 1 0 32.37z"/>
													<path id="x" d="M943.73 1442.83l11.97 11.97"/>
													<path id="y" d="M933.18 1423.32v16.22"/>
													<path id="z" d="M925.08 1431.43h16.22"/>
												</defs>
												<use fill="#fff" fill-opacity="0" stroke="#fff" stroke-miterlimit="50" stroke-width="4" xlink:href="#w" transform="translate(-915 -1413)"/>
												<use fill="#fff" fill-opacity="0" stroke="#fff" stroke-miterlimit="50" stroke-width="6" xlink:href="#x" transform="translate(-915 -1413)"/>
												<use fill="#fff" fill-opacity="0" stroke="#fff" stroke-miterlimit="50" stroke-width="3" xlink:href="#y" transform="translate(-915 -1413)"/>
												<use fill="#fff" fill-opacity="0" stroke="#fff" stroke-miterlimit="50" stroke-width="3" xlink:href="#z" transform="translate(-915 -1413)"/>
											</svg>
										</p>
									</a>
								<?php endwhile; ?>
						<?php endif; ?>
					<?php endwhile; ?>
				<?php endif; ?>
			</div>
		</section>

		<?php get_template_part('template-parts/elements/annies-kits'); ?>
		
	</article>
</main>

<?php get_footer(); ?>