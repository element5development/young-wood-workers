<?php 
/*-------------------------------------------------------------------
    Template Name: Styleguide
-------------------------------------------------------------------*/
?>

<?php get_header(); ?>

<section class="styleguide-title">
	<div class="block">
		<h1><?php echo get_bloginfo( 'name' ); ?></h1>
		<?php if ( get_option('blogdescription') ) : ?>
			<h2><?php echo get_option('blogdescription'); ?></h2>
		<?php endif; ?>
	</div>
</section>

<main class="styleguide-container">
	<div class="brand block">
		<!--  COLORS  -->
		<section class="styleguide-section">
			<h2>Colors palette</h2>
			<div class="colors">
				<p>Primary Colors</p>
				<div class="color">
					<div class="color-swatch"></div>
					<div class="color-details">
						<p class="hex">HEX<span></span></p>
						<p class="rgb">RGB<span></span></p>
						<p class="cmyk">CMYK<span></span></p>
					</div>
				</div>
				<div class="color">
					<div class="color-swatch"></div>
					<div class="color-details">
						<p class="hex">HEX<span></span></p>
						<p class="rgb">RGB<span></span></p>
						<p class="cmyk">CMYK<span></span></p>
					</div>
				</div>
				<div class="color">
					<div class="color-swatch"></div>
					<div class="color-details">
						<p class="hex">HEX<span></span></p>
						<p class="rgb">RGB<span></span></p>
						<p class="cmyk">CMYK<span></span></p>
					</div>
				</div>
				<div class="color">
					<div class="color-swatch"></div>
					<div class="color-details">
						<p class="hex">HEX<span></span></p>
						<p class="rgb">RGB<span></span></p>
						<p class="cmyk">CMYK<span></span></p>
					</div>
				</div>
				<div class="color">
					<div class="color-swatch"></div>
					<div class="color-details">
						<p class="hex">HEX<span></span></p>
						<p class="rgb">RGB<span></span></p>
						<p class="cmyk">CMYK<span></span></p>
					</div>
				</div>
			</div>
			<div class="colors">
				<p>NEUTRALS</p>
				<div class="color">
					<div class="color-swatch"></div>
					<div class="color-details">
						<p class="hex">HEX<span></span></p>
						<p class="rgb">RGB<span></span></p>
						<p class="cmyk">CMYK<span></span></p>
					</div>
				</div>
				<div class="color">
					<div class="color-swatch"></div>
					<div class="color-details">
						<p class="hex">HEX<span></span></p>
						<p class="rgb">RGB<span></span></p>
						<p class="cmyk">CMYK<span></span></p>
					</div>
				</div>
				<div class="color">
					<div class="color-swatch"></div>
					<div class="color-details">
						<p class="hex">HEX<span></span></p>
						<p class="rgb">RGB<span></span></p>
						<p class="cmyk">CMYK<span></span></p>
					</div>
				</div>
				<div class="color">
					<div class="color-swatch"></div>
					<div class="color-details">
						<p class="hex">HEX<span></span></p>
						<p class="rgb">RGB<span></span></p>
						<p class="cmyk">CMYK<span></span></p>
					</div>
				</div>
			</div>
		</section>
		<!--  FONTS -->
		<section class="styleguide-section">
			<h2>FONTS</h2>
			<div class="fonts">
				<div class="font">
					<div class="block">
						<p class="family"></p>
						<p class="example">Aa</p>
						<p class="weight"></p>
					</div>
				</div>
				<div class="font">
					<div class="block">
						<p class="family"></p>
						<p class="example">Aa</p>
						<p class="weight"></p>
					</div>
				</div>
			</div>
		</section>
		<!--  TYPOGRAPHY -->
		<section class="styleguide-section">
			<h2>TYPOGRAPHY</h2>
			<div class="headings">
				<div class="typography">
					<p>H1</p>
					<h1>This is a headline</h1>
				</div>
				<div class="typography">
					<p>H2</p>
					<h2>This is a headline</h2>
				</div>
				<div class="typography">
					<p>H3</p>
					<h3>This is a headline</h3>
				</div>
				<div class="typography">
					<p>H4</p>
					<h4>This is a headline</h4>
				</div>
				<div class="typography">
					<p>H5</p>
					<h5>This is a headline</h5>
				</div>
				<div class="typography">
					<p>Paragraph</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque sit amet pharetra metus. Sed a purus massa. Cras tempus pharetra quam, nec mattis dui semper et. Nunc sapien arcu, bibendum a euismod eget, facilisis quis nisl.</p>
				</div>
			</div>
		</section>
		<!--  LOGOS  -->
		<section class="styleguide-section">
			<h2>Logo</h2>
			<div class="logos">
				<div class="logo">
					<?php $logo_primary = get_field('primary_logo'); ?>
					<a target="_blank" href="<?php echo $logo_primary['url']; ?>">
						<img src="<?php echo $logo_primary['url']; ?>" alt="<?php echo $logo_primary['alt']; ?>" />
					</a>
				</div>
				<?php if( have_rows('logo_variant') ):
					while ( have_rows('logo_variant') ) : the_row(); ?>
						<div class="logo" style="background-color: <?php the_sub_field('background_color'); ?>">
						<?php $logo_variant = get_sub_field('logo'); ?>
						<a target="_blank" href="<?php echo $logo_variant['url']; ?>">
							<img src="<?php echo $logo_variant['url']; ?>" alt="<?php echo $logo_variant['alt']; ?>" />
						</a>
						</div>
					<?php endwhile;
				endif; ?>
			</div>
		</section>
		<!--  ICONS -->
		<?php if( have_rows('icons') ): ?>
			<section class="styleguide-section">
				<h2>ICONS</h2>
				<div class="icons">
					<?php while ( have_rows('icons') ) : the_row(); ?>
						<div class="icon">
							<?php $icon = get_sub_field('icon'); ?>
							<a target="_blank" href="<?php echo $icon['url']; ?>">
								<img src="<?php echo $icon['url']; ?>" alt="<?php echo $icon['alt']; ?>" />
							</a>
						</div>
					<?php endwhile; ?>
				</div>
			</section>
		<?php endif; ?>
		<!--  IMAGERY -->
		<?php if( have_rows('imagery') ): ?>
			<section class="styleguide-section">
				<h2>IMAGERY</h2>
				<div class="images">
					<?php while ( have_rows('imagery') ) : the_row(); ?>
						<?php $image = get_sub_field('image'); ?>
						<a target="_blank" href="<?php echo $image['url']; ?>" class="image" style="background-image: url('<?php echo $image['url']; ?>');"></a>
					<?php endwhile; ?>
				</div>
			</section>
		<?php endif; ?>
	</div>
	<div class="ui-elements block">
		<!--  BUTTONS -->
		<section class="styleguide-section">
			<h2>Buttons</h2>
			<div class="buttons">
				<p>Primary</p>
				<a href="" class="button">button</a>
				<a href="" class="button is-hover">hover</a>
				<a href="" class="button is-active">active</a>
			</div>
			<div class="buttons">
				<p>Secondary</p>
				<a href="" class="button is-secondary">button</a>
				<a href="" class="button is-secondary is-hover">hover</a>
				<a href="" class="button is-secondary is-active">active</a>
			</div>
			<div class="buttons">
				<p>Small Buttons</p>
				<a href="" class="button is-small">small button</a>
				<a href="" class="button is-small is-hover">small hover</a>
				<a href="" class="button is-small is-active">small active</a>
			</div>
			<div class="buttons">
				<p>Ghost</p>
				<a href="" class="button is-ghost">button</a>
				<a href="" class="button is-ghost is-hover">hover</a>
				<a href="" class="button is-ghost is-active">active</a>
			</div>
			<div class="buttons">
				<p>Social Links</p>
				<a target="_blank" href="https://www.facebook.com/" class="social-button facebook">
					<svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
						<path d="M22.676 0H1.324C.594 0 0 .593 0 1.324v21.352C0 23.408.593 24 1.324 24h11.494v-9.294h-3.13v-3.62h3.13V8.41c0-3.1 1.894-4.785 4.66-4.785 1.324 0 2.463.097 2.795.14v3.24h-1.92c-1.5 0-1.793.722-1.793 1.772v2.31h3.584l-.465 3.63h-3.12V24h6.115c.733 0 1.325-.592 1.325-1.324V1.324C24 .594 23.408 0 22.676 0"/>
					</svg>
				</a>
				<a target="_blank" href="https://www.instagram.com/" class="social-button instagram">
					<svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
						<path d="M12 0C8.74 0 8.333.015 7.053.072 5.775.132 4.905.332 4.14.63c-.79.306-1.46.717-2.126 1.384S.934 3.35.63 4.14c-.297.765-.5 1.635-.558 2.913C.012 8.333 0 8.74 0 12s.015 3.667.072 4.947c.06 1.277.26 2.148.558 2.913.306.788.717 1.46 1.384 2.126.667.666 1.336 1.08 2.126 1.384.766.296 1.636.5 2.913.558C8.333 23.988 8.74 24 12 24s3.667-.015 4.947-.072c1.277-.06 2.148-.262 2.913-.558.788-.306 1.46-.718 2.126-1.384.666-.667 1.08-1.335 1.384-2.126.296-.765.5-1.636.558-2.913.06-1.28.072-1.687.072-4.947s-.015-3.667-.072-4.947c-.06-1.277-.262-2.15-.558-2.913-.306-.79-.718-1.46-1.384-2.126C21.32 1.347 20.65.934 19.86.63c-.765-.297-1.636-.5-2.913-.558C15.667.012 15.26 0 12 0zm0 2.16c3.203 0 3.585.016 4.85.07 1.17.056 1.805.25 2.227.416.562.217.96.477 1.382.896.418.42.678.82.895 1.38.164.423.36 1.058.413 2.228.057 1.266.07 1.646.07 4.85s-.015 3.585-.074 4.85c-.06 1.17-.256 1.805-.42 2.227-.225.562-.48.96-.9 1.382-.42.418-.824.678-1.38.895-.42.164-1.065.36-2.235.413-1.275.057-1.65.07-4.86.07-3.21 0-3.586-.015-4.86-.074-1.17-.06-1.815-.256-2.235-.42-.57-.225-.96-.48-1.38-.9-.42-.42-.69-.824-.9-1.38-.164-.42-.358-1.065-.42-2.235-.044-1.26-.06-1.65-.06-4.845 0-3.196.016-3.586.06-4.86.062-1.17.256-1.815.42-2.235.21-.57.48-.96.9-1.38.42-.42.81-.69 1.38-.9.42-.165 1.05-.36 2.22-.42 1.276-.045 1.65-.06 4.86-.06l.045.03zm0 3.678c-3.405 0-6.162 2.76-6.162 6.162 0 3.405 2.76 6.162 6.162 6.162 3.405 0 6.162-2.76 6.162-6.162 0-3.405-2.76-6.162-6.162-6.162zM12 16c-2.21 0-4-1.79-4-4s1.79-4 4-4 4 1.79 4 4-1.79 4-4 4zm7.846-10.405c0 .795-.646 1.44-1.44 1.44-.795 0-1.44-.646-1.44-1.44 0-.794.646-1.44 1.44-1.44.793 0 1.44.646 1.44 1.44z"/>
					</svg>
				</a>
				<a target="_blank" href="https://twitter.com/" class="social-button twitter">
					<svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
						<path d="M23.954 4.57c-.885.388-1.83.653-2.825.774 1.013-.61 1.793-1.574 2.162-2.723-.95.556-2.005.96-3.127 1.185-.896-.96-2.173-1.56-3.59-1.56-2.718 0-4.92 2.204-4.92 4.918 0 .39.044.765.126 1.124C7.69 8.094 4.067 6.13 1.64 3.16c-.427.723-.666 1.562-.666 2.476 0 1.71.87 3.213 2.188 4.096-.807-.026-1.566-.248-2.228-.616v.06c0 2.386 1.693 4.375 3.946 4.828-.413.11-.85.17-1.296.17-.314 0-.615-.03-.916-.085.63 1.952 2.445 3.376 4.604 3.416-1.68 1.32-3.81 2.105-6.102 2.105-.39 0-.78-.022-1.17-.066 2.19 1.394 4.768 2.21 7.557 2.21 9.054 0 14-7.497 14-13.987 0-.21 0-.42-.016-.63.962-.69 1.8-1.56 2.46-2.548l-.046-.02z"/>
					</svg>
				</a>
				<a target="_blank" href="https://www.linkedin.com/" class="social-button linkedin">
					<svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
						<path d="M20.447 20.452h-3.554v-5.57c0-1.327-.027-3.036-1.852-3.036-1.852 0-2.135 1.445-2.135 2.94v5.666H9.35V9h3.415v1.56h.046c.478-.9 1.638-1.85 3.37-1.85 3.602 0 4.268 2.37 4.268 5.456v6.286zM5.337 7.432c-1.144 0-2.063-.925-2.063-2.064 0-1.138.92-2.063 2.063-2.063 1.14 0 2.064.925 2.064 2.063 0 1.14-.924 2.065-2.063 2.065zm1.782 13.02H3.554V9H7.12v11.452zM22.224 0H1.77C.793 0 0 .774 0 1.73v20.54C0 23.228.792 24 1.77 24h20.452C23.2 24 24 23.227 24 22.27V1.73C24 .773 23.2 0 22.222 0h.003z"/>
					</svg>
				</a>
				<a target="_blank" href="https://www.pinterest.com/" class="social-button pinterest">
					<svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
						<path d="M12.017 0C5.397 0 .03 5.367.03 11.987c0 5.08 3.157 9.417 7.617 11.162-.105-.95-.2-2.404.04-3.44.22-.937 1.407-5.957 1.407-5.957s-.36-.72-.36-1.78c0-1.664.968-2.912 2.17-2.912 1.023 0 1.517.77 1.517 1.69 0 1.028-.652 2.566-.99 3.99-.286 1.194.6 2.166 1.774 2.166 2.128 0 3.768-2.245 3.768-5.487 0-2.862-2.063-4.87-5.008-4.87-3.41 0-5.41 2.562-5.41 5.2 0 1.032.395 2.142.89 2.74.1.12.112.225.085.345-.09.375-.294 1.2-.335 1.363-.053.225-.172.27-.4.165-1.496-.69-2.434-2.878-2.434-4.646 0-3.776 2.75-7.252 7.92-7.252 4.16 0 7.393 2.967 7.393 6.923 0 4.135-2.607 7.462-6.233 7.462-1.214 0-2.354-.63-2.758-1.38l-.75 2.85c-.268 1.044-1.003 2.35-1.497 3.145 1.123.345 2.306.535 3.55.535 6.607 0 11.985-5.365 11.985-11.987C23.97 5.39 18.592.026 11.985.026L12.017 0z"/>
					</svg>
				</a>
			</div>
		</section>
		<!-- INPUT FIELDS -->
		<section class="styleguide-section">
			<h2>INPUT FIELDS</h2>
			<?php echo do_shortcode('[gravityform id="2" title="false" description="false"]'); ?>
		</section>
		<!-- LISTS -->
		<section class="styleguide-section">
			<h2>Lists</h2>
			<div class="lists">
				<div class="list">
					<ul>
						<li>Unordered List</li>
						<li>Unordered List</li>
						<li>Unordered List</li>
					</ul>
				</div>
				<div class="list">
					<ol>
						<li>Unordered List</li>
						<li>Unordered List</li>
						<li>Unordered List</li>
					</ol>
				</div>
			</div>
		</section>
		<!-- BLOCKQUOTE -->
		<section class="styleguide-section">
			<h2>Blockquote</h2>
			<div class="quotes">
				<blockquote>
					Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam eu condimentum ex, non tincidunt enim. 
					<span>Quotee</span>
				</blockquote>
			</div>
		</section>
	</div>
</main>

<?php get_footer(); ?>