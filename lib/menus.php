<?php

/*-----------------------------------------
		MENUS - www.wp-hasty.com
-----------------------------------------*/
function nav_creation() {
	$locations = array(
		'primary_nav' => __( 'Primary Navigation'),
		'secondary_nav' => __( 'Secondary Navigation'),
	);
	register_nav_menus( $locations );

}
add_action( 'init', 'nav_creation' );

/*-----------------------------------------
		ADD CLASSES TO A TAG
-----------------------------------------*/
function my_walker_nav_menu_start_el($item_output, $item, $depth, $args) {
	$classes     = implode(' ', $item->classes);
	$item_output = preg_replace('/<a /', '<a class="'.$classes.'"', $item_output, 1);
	return $item_output;
}
add_filter('walker_nav_menu_start_el', 'my_walker_nav_menu_start_el', 10, 4);