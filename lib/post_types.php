<?php

/*-----------------------------------------
	CUSTOM POST TYPES - www.wp-hasty.com
-----------------------------------------*/
// Post Type: testimony
function create_testimony_cpt() {
	$labels = array(
		'name' => __( 'Testimonies', 'Post Type General Name', 'textdomain' ),
		'singular_name' => __( 'testimony', 'Post Type Singular Name', 'textdomain' ),
		'menu_name' => __( 'Testimonies', 'textdomain' ),
		'name_admin_bar' => __( 'testimony', 'textdomain' ),
		'archives' => __( 'testimony Archives', 'textdomain' ),
		'attributes' => __( 'testimony Attributes', 'textdomain' ),
		'parent_item_colon' => __( 'Parent testimony:', 'textdomain' ),
		'all_items' => __( 'All testimonies', 'textdomain' ),
		'add_new_item' => __( 'Add New testimony', 'textdomain' ),
		'add_new' => __( 'Add New', 'textdomain' ),
		'new_item' => __( 'New testimony', 'textdomain' ),
		'edit_item' => __( 'Edit testimony', 'textdomain' ),
		'update_item' => __( 'Update testimony', 'textdomain' ),
		'view_item' => __( 'View testimony', 'textdomain' ),
		'view_items' => __( 'View testimonies', 'textdomain' ),
		'search_items' => __( 'Search testimony', 'textdomain' ),
		'not_found' => __( 'Not found', 'textdomain' ),
		'not_found_in_trash' => __( 'Not found in Trash', 'textdomain' ),
		'featured_image' => __( 'Featured Image', 'textdomain' ),
		'set_featured_image' => __( 'Set featured image', 'textdomain' ),
		'remove_featured_image' => __( 'Remove featured image', 'textdomain' ),
		'use_featured_image' => __( 'Use as featured image', 'textdomain' ),
		'insert_into_item' => __( 'Insert into testimony', 'textdomain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this testimony', 'textdomain' ),
		'items_list' => __( 'testimonies list', 'textdomain' ),
		'items_list_navigation' => __( 'testimonies list navigation', 'textdomain' ),
		'filter_items_list' => __( 'Filter testimonies list', 'textdomain' ),
	);
	$args = array(
		'label' => __( 'testimony', 'textdomain' ),
		'description' => __( '', 'textdomain' ),
		'labels' => $labels,
		'menu_icon' => 'dashicons-format-quote',
		'supports' => array('title', ),
		'taxonomies' => array(),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'show_in_admin_bar' => true,
		'show_in_nav_menus' => false,
		'can_export' => true,
		'has_archive' => false,
		'hierarchical' => false,
		'exclude_from_search' => false,
		'show_in_rest' => true,
		'publicly_queryable' => false,
		'capability_type' => 'post',
	);
	register_post_type( 'testimony', $args );
}
add_action( 'init', 'create_testimony_cpt', 0 );
// Post Type Key: kit
function create_kit_cpt() {
	$labels = array(
		'name' => __( 'Kits', 'Post Type General Name', 'textdomain' ),
		'singular_name' => __( 'Kit', 'Post Type Singular Name', 'textdomain' ),
		'menu_name' => __( 'Kits', 'textdomain' ),
		'name_admin_bar' => __( 'Kit', 'textdomain' ),
		'archives' => __( 'Kit Archives', 'textdomain' ),
		'attributes' => __( 'Kit Attributes', 'textdomain' ),
		'parent_item_colon' => __( 'Parent Kit:', 'textdomain' ),
		'all_items' => __( 'All Kits', 'textdomain' ),
		'add_new_item' => __( 'Add New Kit', 'textdomain' ),
		'add_new' => __( 'Add New', 'textdomain' ),
		'new_item' => __( 'New Kit', 'textdomain' ),
		'edit_item' => __( 'Edit Kit', 'textdomain' ),
		'update_item' => __( 'Update Kit', 'textdomain' ),
		'view_item' => __( 'View Kit', 'textdomain' ),
		'view_items' => __( 'View Kits', 'textdomain' ),
		'search_items' => __( 'Search Kit', 'textdomain' ),
		'not_found' => __( 'Not found', 'textdomain' ),
		'not_found_in_trash' => __( 'Not found in Trash', 'textdomain' ),
		'featured_image' => __( 'Featured Image', 'textdomain' ),
		'set_featured_image' => __( 'Set featured image', 'textdomain' ),
		'remove_featured_image' => __( 'Remove featured image', 'textdomain' ),
		'use_featured_image' => __( 'Use as featured image', 'textdomain' ),
		'insert_into_item' => __( 'Insert into Kit', 'textdomain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Kit', 'textdomain' ),
		'items_list' => __( 'Kits list', 'textdomain' ),
		'items_list_navigation' => __( 'Kits list navigation', 'textdomain' ),
		'filter_items_list' => __( 'Filter Kits list', 'textdomain' ),
	);
	$args = array(
		'label' => __( 'Kit', 'textdomain' ),
		'description' => __( '', 'textdomain' ),
		'labels' => $labels,
		'menu_icon' => 'dashicons-hammer',
		'supports' => array('title', 'custom-fields', ),
		'taxonomies' => array(),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'show_in_admin_bar' => true,
		'show_in_nav_menus' => false,
		'can_export' => true,
		'has_archive' => false,
		'hierarchical' => false,
		'exclude_from_search' => false,
		'show_in_rest' => true,
		'publicly_queryable' => false,
		'capability_type' => 'post',
	);
	register_post_type( 'kit', $args );
}
add_action( 'init', 'create_kit_cpt', 0 );