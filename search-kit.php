<?php 
/*-------------------------------------------------------------------

Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vel
vestibulum erat. Aliquam iaculis lectus sit amet lorem posuere, at
feugiat arcu imperdiet. Nullam tempor, purus quis aliquam luctus,
purus nulla lobortis diam, eget posuere massa quam a diam. Duis
dignissim velit neque, sed faucibus nulla luctus vitae.  

------------------------------------------------------------------*/
?>
<?php 
$args = array( 'post_type' => array('posts', 'kit'));
$args = array_merge( $args, $wp_query->query );
query_posts( $args );
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/pages/header-archive'); ?>

<?php get_template_part('template-parts/navigation/anchor'); ?>

<main>
	<article>
		<div id="default" class="anchor"></div>
	
		<section class="search-feed feed kit-feed default-contents">
			<div class="block">
				<?php get_template_part('template-parts/elements/kits-search'); ?>

				<h3>Search Results for '<?php echo get_search_query() ?>'</h3>
				
				<?php if (!have_posts()) : ?>
					<h4>Sorry we couldn’t find anything related to what you searched for.</h4>
					<p>Try searching for something else or you can fill out the form below with some basic information about your kit and we will do our best to help you out as quickly as we can.</p>
					<?php echo do_shortcode( '[gravityform id=3 title=false description=false]' ); ?>
				<?php else: ?>

				<h5>Kit Instructions</h5>
					<p>Here are the latest instruction PDFs for your Young Woodworkes Kit Club kit. If you don’t see what you are looking for, please <a href="https://www.annieskitclubs.com/join/?program_id=YWW&source=">contact us.</a></p>

					<?php if ( have_posts() ) : ?>
						<?php while ( have_posts() ) : the_post(); ?>
							<?php get_template_part('template-parts/posts/previews/preview', 'kit-instructions'); ?>
						<?php endwhile; ?>
					<?php endif; ?>

				<h5>Project Photos</h5>

					<?php if ( have_posts() ) : ?>
						<?php while ( have_posts() ) : the_post(); ?>
							<?php get_template_part('template-parts/posts/previews/preview', 'kit-photos'); ?>
						<?php endwhile; ?>
					<?php endif; ?>

				<?php endif; ?>
			</div>
			<?php
				the_posts_pagination( array(
					'prev_text'	=> __( 'Previous Page' ),
					'next_text'	=> __( 'Next Page' ),
				) );
			?>
		</section>

	</article>
</main>

<?php get_footer(); ?>