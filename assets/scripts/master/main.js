var $ = jQuery;

$(document).ready(function() {
  /*-------------------------------------------------------------------
		Form input adding & removing classes
	-------------------------------------------------------------------*/
  $('input:not([type=checkbox]):not([type=radio])').focus(function() {
    $(this).addClass('is-activated');
  });
  $('textarea').focus(function() {
    $(this).addClass('is-activated');
  });
  $('select').focus(function() {
    $(this).addClass('is-activated');
  });
  /*------------------------------------------------------------------
  	Element5 Site Credit Appear
  ------------------------------------------------------------------*/
  $('#element5-credit img').viewportChecker({
    classToAdd: 'visible',
    offset: 10,
    repeat: true,
  });
  /*-------------------------------------------------------------------
  	MENU TOGGLE
  -------------------------------------------------------------------*/
  $('#menu-toggle').click(function(evt) {
    evt.stopPropagation();
    $('nav.nav-anchor').toggleClass('is-active');
    $('#menu-toggle').toggleClass('is-active');
  });
  $('body,html').click(function(e) {
    var container = $("nav.nav-anchor");
    if (!container.is(e.target) && container.has(e.target).length === 0) {
      container.removeClass('is-active');
      $('#menu-toggle').removeClass('is-active');
    }
  });
  /*-------------------------------------------------------------------
  	SEARCH TOGGLE
  -------------------------------------------------------------------*/
  $('#search-toggle').click(function(evt) {
    evt.stopPropagation();
    $('#search').toggleClass('is-active');
    $('#search-toggle').toggleClass('is-active');
  });
  $('body,html').click(function(e) {
    var container = $("#search");
    if (!container.is(e.target) && container.has(e.target).length === 0) {
      container.removeClass('is-active');
      $('#search-toggle').removeClass('is-active');
    }
  });
  /*------------------------------------------------------------------
    	MASONRY FOR WP GALLERY
  	------------------------------------------------------------------*/
  $('.landscape').parent().addClass("landscape");
  var galleries = document.querySelectorAll('.gallery');
  for (var i = 0, len = galleries.length; i < len; i++) {
    var gallery = galleries[i];
    initMasonry(gallery);
  }

  function initMasonry(container) {
    var imgLoad = imagesLoaded(container, function() {
      new Masonry(container, {
        itemSelector: '.gallery-item',
        columnWidth: '.gallery-item:first-child',
        percentPosition: true,
        horizontalOrder: true
      });
    });
  }
  /*------------------------------------------------------------------
  	TESTIMONY SLIDER
  ------------------------------------------------------------------*/
  $('.testimonies-slider').slick({
    infinite: true,
    slidesToShow: 2,
    slidesToScroll: 2,
    autoplay: true,
    autoplaySpeed: 3000,
    prevArrow: '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="17" height="31" viewBox="0 0 17 31"><defs><path id="arrow-prev" d="M65.7 1774.24c.25.23.37.51.37.85 0 .34-.12.63-.37.85-.23.23-.5.35-.84.36-.33.01-.6-.1-.83-.36l-13.66-13.79c-.25-.22-.37-.5-.37-.83 0-.33.12-.62.37-.87l13.66-13.8c.23-.22.5-.33.83-.33.33 0 .61.11.84.34.25.25.37.54.37.87 0 .33-.12.6-.37.83l-12.43 12.94z"/></defs><use fill="#fff" xlink:href="#arrow-prev" transform="translate(-50 -1746)"/></svg>',
    nextArrow: '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="17" height="31" viewBox="0 0 17 31"><defs><path id="arrow-next" d="M1375.37 1748.38a1.1 1.1 0 0 1-.37-.85c0-.34.12-.63.37-.85.23-.23.51-.35.84-.36.33-.01.6.1.83.36l13.66 13.79c.25.23.37.5.37.83 0 .33-.12.62-.37.87l-13.66 13.8c-.22.22-.5.33-.83.33-.33 0-.6-.11-.84-.34a1.2 1.2 0 0 1-.37-.87c0-.33.12-.6.37-.83l12.43-12.94z"/></defs><use fill="#fff" xlink:href="#arrow-next" transform="translate(-1375 -1747)"/></svg>',
    responsive: [{
      breakpoint: 550,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
      }
    }, ]
  });
  /*------------------------------------------------------------------
  		MASONRY & ISOTOPE FOR GALLERY PAGE
  	------------------------------------------------------------------*/
  var filterFns = {};
  var $grid = $('.galleries .block').isotope({
    itemSelector: '.gallery-image',
    percentPosition: true,
    sortBy: 'random',
    // layout mode options
    masonry: {
      columnWidth: '.gallery-sizer',
    }
  });
  $grid.imagesLoaded().progress(function() {
    $grid.isotope();
  });
  $('.galleries select').on('change', function() {
    var filterValue = this.value;
    filterValue = filterFns[filterValue] || filterValue;
    $grid.isotope({ filter: filterValue });
  });
  /*------------------------------------------------------------------
  	LIGHTBOX FOR GALLERY PAGE
  ------------------------------------------------------------------*/
  $('.gallery-image').featherlightGallery({
    previousIcon: '«',
    nextIcon: '»',
    galleryFadeIn: 300,
    openSpeed: 300
  });
  /*------------------------------------------------------------------
  	STICKY PORTIONS
	------------------------------------------------------------------*/
  $(window).scroll(function() {
    var scroll = $(window).scrollTop();

    if ($(window).width() > 799) {
      if (scroll > 340) {
        $('.nav-anchor').addClass('is_stuck');
      } else {
        $('.nav-anchor').removeClass('is_stuck');
      }
    }
  });
  /*------------------------------------------------------------------
  	INFINITE SCROLL INIT
  ------------------------------------------------------------------*/
  $('.feed').infiniteScroll({
    path: '.next.page-numbers',
    append: '.feed a',
    button: '.load-more',
    scrollThreshold: false,
    checkLastPage: true,
    status: '.page-load-status',
  });

});