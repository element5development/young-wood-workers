<section class="other-kits-block">
	<?php if( have_rows('kits') ): ?>
		<h3>Other monthly subscription kits by Annie's</h3>
		<?php $count = count(get_field('kits')); ?>
		<div class="other-kits three-col">
			<?php while ( have_rows('kits') ) : the_row(); ?>
				<?php
					$image = get_sub_field('image');
					$alt = $image['alt'];
					$preview = $image['sizes']['small'];
					$link = get_sub_field('button');
				?>
				<a href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>">
					<div class="other-kit">
						<img src="<?php echo $preview; ?>" alt="<?php echo $alt; ?>" />
						<h3><?php the_sub_field('title'); ?></h3>
						<p><?php the_sub_field('description'); ?></p>
						<div class="button" href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>"><?php echo $link['title']; ?></div>
					</div>
				</a>
			<?php endwhile; ?>
		</div>
		<a href="https://www.annieskitclubs.com/" target="_blank">View Annie's Complete Lineup ></a>
	<?php endif; ?>

	<?php if( have_rows('Books') ): ?>
		<h3>Monthly book clubs from Annie's:</h3>
		<?php $count = count(get_field('Books')); ?>
		<div class="other-kits three-col">
			<?php while ( have_rows('Books') ) : the_row(); ?>
				<?php
					$image = get_sub_field('image');
					$alt = $image['alt'];
					$preview = $image['sizes']['small'];
					$link = get_sub_field('button');
				?>
				<a href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>">
					<div class="other-kit">
						<img src="<?php echo $preview; ?>" alt="<?php echo $alt; ?>" />
						<h3><?php the_sub_field('title'); ?></h3>
						<p><?php the_sub_field('description'); ?></p>
						<div class="button" href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>"><?php echo $link['title']; ?></div>
					</div>
				</a>
			<?php endwhile; ?>
		</div>
		<a href="https://www.annieskitclubs.com/" target="_blank">View Annie's Complete Lineup ></a>
	<?php endif; ?>
</section>