<?php 
/*-------------------------------------------------------------------

Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vel
vestibulum erat. Aliquam iaculis lectus sit amet lorem posuere, at
feugiat arcu imperdiet. Nullam tempor, purus quis aliquam luctus,
purus nulla lobortis diam, eget posuere massa quam a diam. Duis
dignissim velit neque, sed faucibus nulla luctus vitae.  

------------------------------------------------------------------*/
?>

<section class="join-now">
	<div  id="join" class="anchor"></div>
	<h2>Join Today</h2>
	<div class="contents">
		<div class="block">
			<?php the_field('column_one'); ?>
		</div>
		<div class="block">
			<?php the_field('column_two'); ?>
		</div>
		<div class="block">
			<?php the_field('column_three'); ?>
		</div>
	</div>
	<?php $join_link = get_field('join_url','option'); ?>
	<a target="<?php echo $join_link['target']; ?>" href="<?php echo $join_link['url']; ?>" class="button">Reserve My Kit</a>
</section>