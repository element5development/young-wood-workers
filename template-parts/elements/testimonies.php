<?php 
/*-------------------------------------------------------------------

Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vel
vestibulum erat. Aliquam iaculis lectus sit amet lorem posuere, at
feugiat arcu imperdiet. Nullam tempor, purus quis aliquam luctus,
purus nulla lobortis diam, eget posuere massa quam a diam. Duis
dignissim velit neque, sed faucibus nulla luctus vitae.  

------------------------------------------------------------------*/
?>

<section class="testimonies">
	<div class="block">
		<h2>WHAT OTHERS HAVE TO SAY</h2>
		<div class="testimonies-slider">
			<?php 
				$args = array(
					'post_type' => array('testimony'),
					'posts_per_page' => -1,
					'ignore_sticky_posts' => true,
					'order' => 'DESC',
				);
				$testimonies = new WP_Query( $args );
			?>
			<?php if ( $testimonies->have_posts() ) { ?>
				<?php while ( $testimonies->have_posts() ) { $testimonies->the_post(); ?>
					<article class="testimony">
						<blockquote>
							<p><?php the_field('quote'); ?></p>
							<p>
								<span><?php the_field('quotee'); ?></span>
								<span><?php the_field('location'); ?></span>
							</p>
						</blockquote>
					</article>
				<?php } ?>
			<?php } ?>
			<?php wp_reset_postdata(); ?>
		</div>
	</div>
</section>