<?php if( have_rows('cards') ): ?>
	<section class="cards">
		<div class="block">
			<?php while ( have_rows('cards') ) : the_row(); ?>
				<?php
					$image = get_sub_field('image');
					$alt = $image['alt'];
					$preview = $image['sizes']['small'];
				?>
				<div class="card">
					<img src="<?php echo $preview; ?>" alt="<?php echo $alt; ?>" />
					<h3><?php the_sub_field('title'); ?></h3>
					<p><?php the_sub_field('description'); ?></p>
				</div>
			<?php endwhile; ?>
		</div>
	</section>
<?php endif; ?>
