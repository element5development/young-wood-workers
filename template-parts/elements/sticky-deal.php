<?php /*

Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vel vestibulum erat. Aliquam iaculis lectus
sit amet lorem posuere, at feugiat arcu imperdiet. Nullam tempor, purus quis aliquam luctus, purus nulla 
lobortis diam, eget posuere massa quam a diam. Duis dignissim velit neque, sed faucibus nulla luctus
vitae.  

*/ ?>
<?php if (get_field('join_url','option')) { ?>
	<section class="sticky-deal">
		<?php $join_link = get_field('join_url','option'); ?>
		<?php if (get_field('promotion_deal','option')) { ?>
			<p class="deal"><?php the_field('promotion_deal','option'); ?></p>
		<?php } ?>
		<?php if (get_field('promotion_description','option')) { ?>
			<p class="description"><?php the_field('promotion_description','option'); ?></p>
		<?php } ?>
		<a target="<?php echo $join_link['target']; ?>" href="<?php echo $join_link['url']; ?>" class="button">Get My Kit</a>
	</section>
<?php } ?>