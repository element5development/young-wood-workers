<?php 
/*-------------------------------------------------------------------

Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vel
vestibulum erat. Aliquam iaculis lectus sit amet lorem posuere, at
feugiat arcu imperdiet. Nullam tempor, purus quis aliquam luctus,
purus nulla lobortis diam, eget posuere massa quam a diam. Duis
dignissim velit neque, sed faucibus nulla luctus vitae.  

------------------------------------------------------------------*/
?>

<?php if( have_rows('split_ctas') ) : ?>
	<div class="split-block">
		<div id="split" class="anchor"></div>
		<?php if ( get_field('heading_split_cta') ) { ?>
			<h2><?php the_field('heading_split_cta') ?></h2>
		<?php } ?>
		<?php while ( have_rows('split_ctas') ) : the_row(); ?>

			<?php 
				$image = get_sub_field('image'); 
				$button = get_sub_field('button');
			?>

			<section class="split-cta">
				<div class="image" style="background-image: url(<?php echo $image['url']; ?>);"></div>
				<div class="content">
					<div class="block">
						<h3><?php the_sub_field('heading'); ?></h3>
						<p><?php the_sub_field('description'); ?></p>
						<a target="<?php echo $button['target']; ?>" href="<?php echo $button['url']; ?>" class="button"><?php echo $button['title']; ?></a>
					</div>
				</div>
			</section>

		<?php endwhile; ?>
	</div>
<?php endif; ?>