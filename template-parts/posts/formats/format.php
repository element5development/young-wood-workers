<?php 
/*-------------------------------------------------------------------

This is the default post format.
The other formats are SUPER basic so you can style them as you like.

Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vel
vestibulum erat. Aliquam iaculis lectus sit amet lorem posuere, at
feugiat arcu imperdiet. Nullam tempor, purus quis aliquam luctus,
purus nulla lobortis diam, eget posuere massa quam a diam. Duis
dignissim velit neque, sed faucibus nulla luctus vitae.  

------------------------------------------------------------------*/
?>

<article id="post-<?php the_ID(); ?>" class="single-post">
	<section class="default-contents">
    <?php the_content(); ?>
  </section>
</article>