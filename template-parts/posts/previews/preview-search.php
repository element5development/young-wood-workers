<?php 
/*-------------------------------------------------------------------

Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vel
vestibulum erat. Aliquam iaculis lectus sit amet lorem posuere, at
feugiat arcu imperdiet. Nullam tempor, purus quis aliquam luctus,
purus nulla lobortis diam, eget posuere massa quam a diam. Duis
dignissim velit neque, sed faucibus nulla luctus vitae.  

------------------------------------------------------------------*/
?>
<?php if(get_post_type() == 'kit'): ?>
	<article class="post-preview-kit">

		<?php
		if( have_rows('instruction_documentation') ):
			while ( have_rows('instruction_documentation') ) : the_row();
				$title = get_sub_field('title');
				$number = get_sub_field('revision_number');
				$download = get_sub_field('download');
				?>
				<a href="<?php echo $download; ?>">
					<div class="pdf-card">
						<svg width="137" height="153">
							<defs>
								<path id="k" d="M537.5 1692.79h105.56V1559.6H537.5z"/>
								<path id="l" d="M550.68 1701.78h105.56V1568.6H550.68z"/>
								<path id="m" d="M557.47 1693.44h97.25v-123.45h-97.25z"/>
								<path id="n" d="M565.25 1685.67h105.56V1552.5H565.25z"/>
								<path id="o" d="M576.1 1606.13h84.82v-29.15H576.1z"/>
								<path id="p" d="M576.1 1571.5h84.82v-8.85H576.1z"/>
								<path id="g" d="M576.1 1640.8h35.74v-29.15H576.1z"/>
								<path id="h" d="M576.1 1656.7h84.82v-8.72H576.1z"/>
								<path id="i" d="M576.1 1670.7h84.82v-8.72H576.1z"/>
								<path id="j" d="M625.19 1640.8h35.73v-29.15H625.2z"/>
							</defs>
							<use fill="#7c9eb2" xlink:href="#k" transform="translate(-536 -1551)"/>
							<use fill="#fff" fill-opacity="0" stroke="#34668a" stroke-miterlimit="50" stroke-width="3" xlink:href="#k" transform="translate(-536 -1551)"/>
							<use fill="#fff" xlink:href="#l" transform="translate(-536 -1551)"/>
							<use fill="#fff" fill-opacity="0" stroke="#34668a" stroke-miterlimit="50" stroke-width="3" xlink:href="#l" transform="translate(-536 -1551)"/>
							<use fill="#7c9eb2" xlink:href="#m" transform="translate(-536 -1551)"/>
							<use fill="#fff" xlink:href="#n" transform="translate(-536 -1551)"/>
							<use fill="#fff" fill-opacity="0" stroke="#34668a" stroke-miterlimit="50" stroke-width="3" xlink:href="#n" transform="translate(-536 -1551)"/>
							<use fill="#7c9eb2" xlink:href="#o" transform="translate(-536 -1551)"/>
							<use fill="#f5d548" xlink:href="#p" transform="translate(-536 -1551)"/>
							<use fill="#34668a" xlink:href="#g" transform="translate(-536 -1551)"/>
							<use fill="#34668a" xlink:href="#h" transform="translate(-536 -1551)"/>
							<use fill="#34668a" xlink:href="#i" transform="translate(-536 -1551)"/>
							<use fill="#34668a" xlink:href="#j" transform="translate(-536 -1551)"/>
						</svg>
						<h5><?php echo $title; ?></h5>
						<p>version <?php echo $number; ?></p>
						<a href="<?php echo $download; ?>" class="button">Download PDF</a>
					</div>
				</a>
				<?php
			endwhile;
		else :
			// no rows found
		endif;
		?>

	</article>
	<hr>
	<?php else: ?>
	<a href="<?php the_permalink(); ?>">
		<article class="post-preview">
			<header>
				<h2><?php the_title(); ?></h2>
			</header>
			<?php the_excerpt(); ?>
		</article>
	</a>
	<hr>
<?php endif; ?>