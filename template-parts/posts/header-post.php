<?php 
/*-------------------------------------------------------------------

Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vel
vestibulum erat. Aliquam iaculis lectus sit amet lorem posuere, at
feugiat arcu imperdiet. Nullam tempor, purus quis aliquam luctus,
purus nulla lobortis diam, eget posuere massa quam a diam. Duis
dignissim velit neque, sed faucibus nulla luctus vitae.  

------------------------------------------------------------------*/
?>

<?php 
	if ( get_field('background_image_single') ) {
		$background = get_field('background_image_single');
		$backgroundURL = $background['url'];
	} else {
		$backgroundURL = get_stylesheet_directory_uri() . '/dist/images/default-header.png';
	}
?>

<section class="page-title title-section" style="background-image: url(<?php echo $backgroundURL; ?>);">
	<div class="block">
		<?php if ( get_field('title') ) { ?>
			<h1><?php the_field('title'); ?></h1>
		<?php } else { ?>
			<h1><?php the_title(); ?></h1>
		<?php } ?>
		<time class="entry-time" datetime="<?php get_the_time('Y-m-d'); ?>">
			<?php echo get_the_time(get_option('date_format')); ?>
		</time>
		<p class="categories">
			<?php echo get_the_category_list(', ') ?>
		</p>
	</div>
</section>