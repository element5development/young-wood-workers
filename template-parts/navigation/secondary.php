<nav class="secondary-nav">
	<?php if (has_nav_menu('secondary_nav')) :
		wp_nav_menu(['theme_location' => 'secondary_nav']);
	endif; ?>
</nav>